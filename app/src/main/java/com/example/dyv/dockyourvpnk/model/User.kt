package com.example.dyv.dockyourvpnk.model

import java.text.SimpleDateFormat
import java.util.*

data class UserWithParameter(
    val firstname: String,
    val lastname: String,
    val username: String,
    val password: String)

data class User(
    var firstname: String = "",
    var lastname: String = "",
    var username: String = "",
    var password: String = "")
