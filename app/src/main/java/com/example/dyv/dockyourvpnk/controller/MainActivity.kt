package com.example.dyv.dockyourvpnk.controller

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.dyv.dockyourvpnk.R
import com.example.dyv.dockyourvpnk.model.User
import com.example.dyv.dockyourvpnk.service.ApiService
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        activity_main_password_login.isCursorVisible = false

        activity_main_button_login.isClickable = false

        activity_main_button_login.setOnClickListener {
            toast(activity_main_pseudo_login.text.toString())
        }

        val retrofit = Retrofit.Builder()
            .baseUrl("http://35.205.202.70/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create<ApiService>(ApiService::class.java!!)
        val executor = Executors.newSingleThreadExecutor()

        executor.execute{
            service.getUserList().enqueue(object : retrofit2.Callback<List<User>> {
                override fun onFailure(call: Call<List<User>>, t: Throwable) {
                    activity_main_test.text = "Erreur réseau"
                }

                override fun onResponse(call: Call<List<User>>, response: Response<List<User>>){
                    val userConnected = User()
                    userConnected.firstname = response.body()?.get(0)!!.firstname
                    activity_main_test.text = userConnected.firstname
                }
            })
        }
        toast("Ok")
        }


    fun Context.toast(message: CharSequence) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

}

