package com.example.dyv.dockyourvpnk.service

import com.example.dyv.dockyourvpnk.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("user")
    fun getCheckUserConnexion(): Call<User>

    @GET("listUser")
    fun getUserList(): Call<List<User>>
}